package com.globalhack.services.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;

/**
 * Domain class for customer object
 * 
 * @author Himanshu Sahni
 *
 */
@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// personal attributes
	private String firstName;
	private String lastName;
	private String gender;
	private String maritalStatus;
	private String race;
	private String address;
	private String email;
	private int familySize;
	private String phoneNumber;
	private String state;

	// Source information
	private String status;
	private String source;
	private String sourceCategory;

	private int numOfPaymentsMissed;
	private Long avgMonthlyBill;
	private Long percentIncreaseInBill;

	private String comments;

	// Other information
	private Boolean isVeteran = false;
	private Boolean isDisabled = false;
	private Boolean isPregnant = false;
	private Boolean isAbused = false;
	private Boolean isEmployed = false;

	private Boolean becameHomeless = false;

	@Column(name = "create_ts", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createTs = new Timestamp(new java.util.Date().getTime());

	@Column(name = "update_ts", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp updateTs = new Timestamp(new java.util.Date().getTime());

	transient private Long score;

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Timestamp getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Timestamp createTs) {
		this.createTs = createTs;
	}

	public Timestamp getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Timestamp updateTs) {
		this.updateTs = updateTs;
	}

	public Long getAvgMonthlyBill() {
		return avgMonthlyBill;
	}

	public void setAvgMonthlyBill(Long avgMonthlyBill) {
		this.avgMonthlyBill = avgMonthlyBill;
	}

	public Long getPercentIncreaseInBill() {
		return percentIncreaseInBill;
	}

	public void setPercentIncreaseInBill(Long percentIncreaseInBill) {
		this.percentIncreaseInBill = percentIncreaseInBill;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public String getSourceCategory() {
		return sourceCategory;
	}

	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}

	public int getNumOfPaymentsMissed() {
		return numOfPaymentsMissed;
	}

	public void setNumOfPaymentsMissed(int numOfPaymentsMissed) {
		this.numOfPaymentsMissed = numOfPaymentsMissed;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Client(String firstName, String lastName, String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	public Client() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean getIsVeteran() {
		return isVeteran;
	}

	public void setIsVeteran(Boolean isVeteran) {
		this.isVeteran = isVeteran;
	}

	public Boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Boolean getIsPregnant() {
		return isPregnant;
	}

	public void setIsPregnant(Boolean isPregnant) {
		this.isPregnant = isPregnant;
	}

	public Boolean getIsAbused() {
		return isAbused;
	}

	public void setIsAbused(Boolean isAbused) {
		this.isAbused = isAbused;
	}

	public Boolean getIsEmployed() {
		return isEmployed;
	}

	public void setIsEmployed(Boolean isEmployed) {
		this.isEmployed = isEmployed;
	}

	public Boolean getBecameHomeless() {
		return becameHomeless;
	}

	public void setBecameHomeless(Boolean becameHomeless) {
		this.becameHomeless = becameHomeless;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
