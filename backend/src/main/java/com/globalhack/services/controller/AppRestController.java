package com.globalhack.services.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.*;
import java.util.regex.Pattern;

import javax.validation.Valid;

import com.globalhack.services.dao.ServicesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.globalhack.services.model.Client;
import com.globalhack.services.model.ModelDto;
import com.globalhack.services.repositories.ClientRepository;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSequence;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.LabelSequence;

/**
 * Rest Controller of the application where all REST HTTP methods are defined
 * and implemented
 *
 * @author Himanshu Sahni
 *
 */
@RestController
@RequestMapping("/clients")
@CrossOrigin
public class AppRestController {

	/**
	 * Customer Repository will be autowired once application starts
	 */
	@Autowired
	private ClientRepository repository;

	@Autowired
	private ServicesDao servicesDao;

	/**
	 * GET method to get all customers from the database
	 *
	 * @return - A list of customers in the database
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Client>> getAllUsers() {

		List<Client> clientList = (List<Client>) repository.findAll();

		for (int i = 0; i < clientList.size(); i++) {
			clientList.get(i).setScore(getRandomNumber());
		}

		// return new ResponseEntity<>((Collection<Client>)
		// repository.findAll(), HttpStatus.OK);
		return new ResponseEntity<>((Collection<Client>) clientList, HttpStatus.OK);
	}

	private static long getRandomNumber() {
		Random r = new Random();
		int Low = 1;
		int High = 100;
		int result = r.nextInt(High-Low) + Low;

		return result;
	}

	@RequestMapping(value = "/status={status}", method = RequestMethod.GET)
	public ResponseEntity<Collection<Client>> getAllUsersWithStatus(@PathVariable String status) {

		List<Client> clientList = (List<Client>) repository.findAll();

		List<Client> filteredClientList = new ArrayList<Client>();

		for (int i = 0; i < clientList.size(); i++) {

			Client client = clientList.get(i);
			client.setScore(getRandomNumber());
			if (client.getStatus().equalsIgnoreCase(status)) {
				filteredClientList.add(client);
			}
		}

		// return new ResponseEntity<>((Collection<Client>)
		// repository.findAll(), HttpStatus.OK);
		return new ResponseEntity<>((Collection<Client>) filteredClientList, HttpStatus.OK);
	}

	/**
	 * GET method to retrieve a customer based on customer id
	 *
	 * @param
	 *            id
	 * @return customer information
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Client> getUserWithId(@PathVariable Long id) {
		return new ResponseEntity<>(repository.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/services/{input}")
	public ResponseEntity<List<Map<String, Object>>> getServices(@PathVariable String input) {

		List<Map<String, Object>> services = servicesDao.getServices(input);
		return new ResponseEntity<>(services, HttpStatus.OK);
	}

	/*  *//**
			 * GET method to retrieve a customer based on customer name
			 *
			 * @param Customer
			 *            name
			 * @return customer information
			 *//*
			 * @RequestMapping(method = RequestMethod.GET, params = { "name" })
			 * public ResponseEntity<Collection<Client>>
			 * findUserWithName(@RequestParam(value = "name") String name) {
			 * return new ResponseEntity<>(repository.findByName(name),
			 * HttpStatus.OK); }
			 */

	/**
	 * POST method to add a new customer in the database
	 *
	 * @param
	 * @return Customer information that is added
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> addUser(@RequestBody Client input) {
		return new ResponseEntity<>(repository.save(input), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateChannels(@Valid @RequestBody Client clientDTO) throws URISyntaxException {
		// log.debug("REST request to update Channels : {}", channelsDTO);
		if (clientDTO.getId() == null) {
			return addUser(clientDTO);
		}

		return new ResponseEntity<>(repository.save(clientDTO), HttpStatus.OK);
		/*
		 * Client result = repository.save(clientDTO); return
		 * ResponseEntity.ok()
		 * .headers(HeaderUtil.createEntityUpdateAlert("channels",
		 * clientDTO.getId().toString())) .body(result);
		 */
	}

	/**
	 * DELETE method to delete a customer from the database
	 *
	 * @param
	 *            id
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteUser(@PathVariable Long id) {
		repository.delete(id);
	}

	private ParallelTopicModel model = null;
	private List<List<String>> myTopics = new ArrayList<>();
	private InstanceList instances = null;
	private Map<Integer,Integer> mappingToTopics = new HashMap<>();
	@RequestMapping(value = "/model", method = RequestMethod.POST)
	public ResponseEntity<List<List<String>>> model(@RequestBody ModelDto queryDto) throws Exception {
		mappingToTopics = new HashMap<>();
		for(int i=0;i<queryDto.noOfTopics;i++){
			mappingToTopics.put(i,0);
		}
		myTopics.clear();
		String fName = queryDto.fileName;
		String folder = queryDto.path;
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
		pipeList.add(new CharSequenceLowercase());
		pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
		pipeList.add(new TokenSequenceRemoveStopwords(new File(folder + "/stoplists/", "en.txt"), "UTF-8", false, false,
				false));
		pipeList.add(new TokenSequence2FeatureSequence());

		this.instances = new InstanceList(new SerialPipes(pipeList));

		Reader fileReader = new InputStreamReader(new FileInputStream(new File(folder + fName)), "UTF-8");
		instances
				.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"), 3, 2, 1)); // data,
		// label,
		// name
		// fields

		// Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
		// Note that the first parameter is passed as the sum over topics, while
		// the second is the parameter for a single dimension of the Dirichlet
		// prior.
		int numTopics = queryDto.noOfTopics;
		this.model = new ParallelTopicModel(numTopics, 1.0, 0.01);
		model.addInstances(instances);

		// Use two parallel samplers, which each look at one half the corpus and
		// combine
		// statistics after every iteration.
		model.setNumThreads(4);

		// Run the model for 50 iterations and stop (this is for testing only,
		// for real applications, use 1000 to 2000 iterations)
		model.setNumIterations(1000);
		model.estimate();

		// Show the words and topics in the first instance

		// The data alphabet maps word IDs to strings
		Alphabet dataAlphabet = instances.getDataAlphabet();

		FeatureSequence tokens = (FeatureSequence) model.getData().get(0).instance.getData();
		LabelSequence topics = model.getData().get(0).topicSequence;

		Formatter out = new Formatter(new StringBuilder(), Locale.US);
		for (int position = 0; position < tokens.getLength(); position++) {
			out.format("%s-%d ", dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)),
					topics.getIndexAtPosition(position));
		}
		// Get an array of sorted sets of word ID/count pairs
		ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

		List<String> topicCSVLines = new ArrayList<String>();
		String l1 = "TopicId,Words";
		topicCSVLines.add(l1);

		// Show top 5 words in topics with proportions for the first document
		for (int topic = 0; topic < numTopics; topic++) {
			Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();

			out = new Formatter(new StringBuilder(), Locale.US);
			// out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
			out.format("%d,", topic);
			int rank = 0;
			List<String> words = new ArrayList<>();
			while (iterator.hasNext() && rank < 6) {
				IDSorter idCountPair = iterator.next();
				out.format(",%s (%.0f)", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
				words.add(dataAlphabet.lookupObject(idCountPair.getID()).toString());
				rank++;
			}
			myTopics.add(words);
			// System.out.println(out);
			//topicCSVLines.add(out.toString());
		}
		/*
		for (String s : topicCSVLines) {
			System.err.println(s);
		}
		*/

		for(int i=0;i<instances.size();i++){
			String name = model.getData().get(i).instance.getName().toString();
			double[] ta = model.getTopicProbabilities(i);
			for(int x=0;x<queryDto.noOfTopics;x++){
				if(ta[x]>=0.3){
					int score = mappingToTopics.get(x);
					score++;
					mappingToTopics.put(x,score);
				}
			}

		}
		return new ResponseEntity<>(myTopics, HttpStatus.OK);
	}

	@RequestMapping(value = "/model", method = RequestMethod.GET)
	public ResponseEntity<List<List<String>>> model() throws Exception {
		return new ResponseEntity<>(myTopics, HttpStatus.OK);
	}
	@RequestMapping(value = "/modelDistribution", method = RequestMethod.GET)
	public ResponseEntity<Map<Integer,Integer>> modelDistribution() throws Exception {
		return new ResponseEntity<Map<Integer,Integer>>(mappingToTopics, HttpStatus.OK);
	}

	@RequestMapping(value = "/predict", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> predict(@Valid @RequestBody String data) throws Exception {
		List<Double> affinity = new ArrayList<>();
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
		pipeList.add(new CharSequenceLowercase());
		pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
		pipeList.add(new TokenSequence2FeatureSequence());

		// InstanceList instances = new InstanceList(new SerialPipes(pipeList));
		InstanceList testing = new InstanceList(instances.getPipe());

		TopicInferencer inferencer = model.getInferencer();

		// System.out.println("0\t" + testProbabilities[0]);

		// InstanceList testing = new InstanceList(instances.getPipe());
		// testing.addThruPipe(new Instance(topicZeroText.toString(), null,
		// "test instance", null));
		// testing.addThruPipe(instances.get(0));

		// TopicInferencer inferencer = model.getInferencer();
		// double[] testProbabilities =
		// inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
		// String data = "UUID 1952 RACE_A Gender_Male VeteranStatus disabled";
		testing.addThruPipe(new Instance(data, null, "test instance", null));
		double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 1000, 100, 500);
		affinity.add(testProbabilities[0]);
		affinity.add(testProbabilities[1]);
		affinity.add(testProbabilities[2]);
		affinity.add(testProbabilities[3]);
		affinity.add(testProbabilities[4]);

		System.out.println("0\t" + testProbabilities[0]);
		System.out.println("1\t" + testProbabilities[1]);
		System.out.println("2\t" + testProbabilities[2]);
		System.out.println("3\t" + testProbabilities[3]);
		System.out.println("4\t" + testProbabilities[4]);
		Map<String, Object> ret = new HashMap<>();
		ret.put("MODEL", this.myTopics);
		ret.put("AFFINITY", testProbabilities);
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/metrics/{id}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getUserMetrics(@PathVariable Long id) throws Exception {

		Client clientDTO = repository.findOne(id);

		String payLoad = getPayLoad(clientDTO);

		return predict(payLoad);

	}

	private static String getPayLoad(Client clientDTO) {

		StringBuilder payLoad = new StringBuilder();

		if (clientDTO.getRace().equalsIgnoreCase("black")) {
			payLoad.append("RACE_A".toLowerCase());
			payLoad.append(" ");
		}
		if (clientDTO.getGender().equalsIgnoreCase("male")) {
			payLoad.append("Gender_Male".toLowerCase());
			payLoad.append(" ");
		} else if (clientDTO.getGender().equalsIgnoreCase("female")) {
			payLoad.append("Gender_FeMale".toLowerCase());
			payLoad.append(" ");
		}
		if (clientDTO.getIsVeteran()) {
			payLoad.append("VeteranStatus".toLowerCase());
			payLoad.append(" ");
		} else {
			payLoad.append("NonVeteran".toLowerCase());
			payLoad.append(" ");
		}
		if (clientDTO.getIsDisabled()) {
			payLoad.append("disabled".toLowerCase());
			payLoad.append(" ");
		}
		if (!clientDTO.getIsEmployed()) {
			payLoad.append("UNEMPLOYED".toLowerCase());
			payLoad.append(" ");
		}
		if (clientDTO.getIsAbused()) {
			payLoad.append("DomesticViolenceVictim".toLowerCase());
			payLoad.append(" ");
		}

		System.out.println("PAYLOAD STRING IS " + payLoad.toString());

		return payLoad.toString();

	}

}
