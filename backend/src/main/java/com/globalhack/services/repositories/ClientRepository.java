package com.globalhack.services.repositories;

import org.springframework.data.repository.CrudRepository;

import com.globalhack.services.model.Client;

/**
 * Interface that extends CrudRepository that allows CRUD (Create, Retrieve,
 * Update, Delete) operations on customer model object
 * 
 * @author Himanshu Sahni
 *
 */
public interface ClientRepository extends CrudRepository<Client, Long> {

}
