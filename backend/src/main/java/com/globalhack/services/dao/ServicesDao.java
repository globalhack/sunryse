package com.globalhack.services.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

import static net.didion.jwnl.data.PointerType.CATEGORY;

/**
 * Created by himanshu on 10/23/2016.
 */
@Repository
public class ServicesDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Map<String, Object>> getServices(String inputString) {
        return this.jdbcTemplate.queryForList(sql, inputString);

    }


    private String sql = "SELECT SERVICES FROM SERVICES_MAPPING WHERE ? " +
    "like CONCAT('%',UPPER(CATEGORY),'%')";}
