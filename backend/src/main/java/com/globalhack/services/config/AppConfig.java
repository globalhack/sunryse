package com.globalhack.services.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@ComponentScan
public class AppConfig {

	@Bean
	public JdbcTemplate getJdbcTemplate(BasicDataSource basicDataSource) {
		return new JdbcTemplate(basicDataSource);
	}

	@Bean
	public BasicDataSource dataSource(@Value("${spring.datasource.url}") String url,
			@Value("${spring.datasource.username}") String userName,
			@Value("${spring.datasource.password}") String passWord) {
		BasicDataSource basicDataSource = new BasicDataSource();

		basicDataSource.setUrl(url);
		basicDataSource.setUsername(userName);
		basicDataSource.setPassword(passWord);
		return basicDataSource;

	}
}