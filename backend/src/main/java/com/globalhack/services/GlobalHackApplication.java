package com.globalhack.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class to start the spring boot REST APP
 *** 
 * @author Himanshu Sahni
 *
 */
@SpringBootApplication
public class GlobalHackApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlobalHackApplication.class, args);
	}

}
