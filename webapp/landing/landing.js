angular.module('sample.landing', [
  'ui.router',
  'angular-storage',
  'angular-jwt'

])
.config(function ($stateProvider) {
        $stateProvider.state('landing', {
            url: '/',
            controller: 'LandingCtrl',
            templateUrl: 'landing/landing.html',
            data: {
                requiresLogin: false
            }
        });
    })
.controller('LandingCtrl', function HomeController($scope, $http, store, jwtHelper) {
        /*$scope.jwt = store.get('jwt');
        $scope.decodedJwt = $scope.jwt && jwtHelper.decodeToken($scope.jwt);
        $scope.isAdmin = store.get('isAdmin');
        $scope.uid = store.get('uid');*/
});

