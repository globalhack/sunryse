angular.module( 'sample.login', [
  'ui.router',
  'angular-storage',
  'angular-jwt'
])
.config(function($stateProvider) {
  $stateProvider.state('login', {
    url: '/login',
    controller: 'LoginCtrl',
    templateUrl: 'login/login.html'
  });
})
.controller( 'LoginCtrl', function LoginController( $scope, $http, store, $state, jwtHelper) {
   
  $scope.user = {};
  
  $scope.login = function() {
    $state.go('home');
    //location.href='index.html#/';
  }

});