angular.module('sample.home', [
  'ui.router'
])
    .config(function ($stateProvider) {
        $stateProvider.state('home', {
            url: '/dashboard',
            controller: 'HomeCtrl',
            templateUrl: 'home/home.html',
            data: {
                requiresLogin: false
            }
        });
    })
    .controller('HomeCtrl', function HomeController($scope, $http, store, jwtHelper, $timeout) {
        
        /*   insight **/

        var diameter = 700,
            format = d3.format(",d"),
            color = d3.scale.category20c();
        var bubble = d3.layout.pack()
            .sort(null)
            .size([diameter, diameter])
            .padding(1.5);

        var svg = d3.select("#d3chart").append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");  

        var tooltip = d3.select("body")
            .append("div")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("color", "white")
            .style("padding", "8px")
            .style("background-color", "rgba(0, 0, 0, 0.75)")
            .style("border-radius", "6px")
            .style("font", "12px sans-serif")
            .text("tooltip");

            d3.json("detail/data.json", function(error, root) {
                var node = svg.selectAll(".node")
                    .data(bubble.nodes(root)
                    .filter(function(d) { return !d.children; }))
                .enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

                node.append("circle")
                    .attr("r", function(d) { return d.r; })
                    .style("fill", function(d) { return color(d.value); })
                    .on("mouseover", function(d) {
                            tooltip.html(getTitle(d.label));
                            tooltip.style("visibility", "visible");
                    })
                    .on("mousemove", function() {
                        return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
                    })
                    .on("mouseout", function(){return tooltip.style("visibility", "hidden");});

                node.append("text")
                    .attr("dy", ".3em")
                    .style("text-anchor", "middle")
                    .style("pointer-events", "none")
                    .text(function(d) { return d.value; });
            });

            function getTitle(attributesArray){
                attributesArray = transformModel([attributesArray])[0];
                var html = "";
                _.each(attributesArray, function(attribute){
                    html = html+attribute+"<br/>"
                })
                return html;
            }



            $scope.insightData = {
                labels: [],
                data: [],
                options: {
                    tooltips: {
                        callbacks: {
                            title: function (tooltipItem, dataObject) {
                                var index = tooltipItem[0].index;
                                var labels = $scope.insightData.labels[index];
                                return labels;
                            }
                        }
                    },
                    scales: {
                        xAxes: [{
                            display: false
            }],
                        yAxes: [{
                            display: false
            }]
                    }
                }

            }
            buildInsightData();

            function buildInsightData() {
                var insightData = [];
                var url = APIHOST + "clients/model";
                $http.get(url, {}).then(function (response) {
                    var attributesList = response.data;
                    url = APIHOST + "clients/modelDistribution";
                    $http.get(url, {}).then(function (response) {
                        var attributesSizes = _.values(response.data);
                        var attributeTotal = _.sum(attributesSizes);
                        attributesSizes = _.map(attributesSizes, function (attributeSize) {
                            return Math.round((attributeSize / attributeTotal) * 100);
                        });
                        attributesSizes = _.sortBy(attributesSizes, function (attributeSize) {
                            return attributeSize * -1;
                        });
                        var points = [];
                        var point = attributesSizes[0];
                        var radius = Math.round(point / 2);
                        var sizeFactor = 7;
                        points.push({
                            x: 100,
                            y: 100,
                            r: radius * sizeFactor
                        });
                        var otherPoint = attributesSizes[1];
                        points.push({
                            x: 100 + radius + otherPoint / 2 ,
                            y: 100,
                            r: (otherPoint / 2) * sizeFactor
                        });
                        otherPoint = attributesSizes[2];
                        points.push({
                            x: 100,
                            y: 100 + radius + otherPoint /2,
                            r: (otherPoint / 2) * sizeFactor
                        });
                        otherPoint = attributesSizes[3];
                        points.push({
                            x: 100 - radius - otherPoint / 2,
                            y: 100,
                            r: (otherPoint / 2) * sizeFactor
                        });
                        otherPoint = attributesSizes[4];
                        points.push({
                            x: 100,
                            y: 100 - radius - otherPoint,
                            r: (otherPoint / 2) * sizeFactor
                        });

                        $scope.insightData.data = points;
                        $scope.insightData.labels = attributesList;

                    })
                }, function () {});

            }
            /* end of insight **/
    
    
        $scope.tab = 1;

        $scope.setTab = function (tabId) {
            $scope.tab = tabId;
        };

        $scope.isSet = function (tabId) {
            return $scope.tab === tabId;
        };

        $scope.api_host = APIHOST;
        $scope.radarData = {
            labels: [],
            data: [],
            options: {
                tooltips: {
                    mode: "single",
                    callbacks: {
                        title: function (tooltipItem, dataObject) {
                            var index = tooltipItem[0].index;
                            var labels = $scope.radarData.longLabels[index];
                            return labels;
                        }
                    }
                },
            },
            colors: ['#48d68d']
        }


        $scope.sendEmail = function (userId) {
            //https://testapi.taxy.tips/api/email/{userId};
            $http({
                url: 'https://testapi.taxy.tips/api/email/' + userId,
                method: 'GET'
                    //data: {"duration" : 7}
            }).then(function (response) {
                console.log(response);
            });
        }
        $scope.showDetail = function (userId) {
            buildRadarChart(userId);
            buildClientData(userId);

        }

        $scope.listHistory = function (clientType) {
            $scope.listClients(clientType);
        }

        function buildClientData(userId) {
            var url = APIHOST + "clients/" + userId;
            $http.get(url, {}).then(function (response) {
                $scope.userData = response.data;
                console.log($scope.userData);
            }, function () {});
        }

        function transformModel(model) {
            var mapping = {
                "domesticviolencevictim": "Domestic Violence Victim",
                "gender_female": "Female",
                "gender_male": "Male",
                "disabled": "Disabled",
                "uuid": "UUID",
                "race_a": "Race - Black",
                "pregnancystatusdoesnotknow": "Pregnancy",
                "race_b": "Race - Non-Black",
                "generalhealthstatusok": "Health OK",
                "unemployed": "Unemployed",
                "generalhealthstatusgood": "Health Good",
                "veteranstatus": "Veteran",
                "nonveteran": "Non Veteran",
                "employed": "Employed"
            }
            var newModel = [];
            angular.forEach(model, function (modelItem) {
                var newArray = [];
                angular.forEach(modelItem, function (mappingItem) {
                    newArray.push(mapping[mappingItem]);
                })
                newModel.push(newArray);
            });
            return newModel;
        }        

        function buildRadarChart(userId) {
            var url = APIHOST + "clients/metrics/" + userId;
            $http.get(url, {}).then(function (response) {
                var data = response.data;
                $scope.metricsData = data;
                //console.log($scope.metricsData);
                var points = [];
                angular.forEach(data.AFFINITY, function (item) {
                    points.push(Math.round(item * 100));
                })
                $scope.radarData.data = points;
                //console.log($scope.radarData.data);
                var model = transformModel(data.MODEL);
                var labels = [];
                var dataMap = [];
                var maxscore = 0;
                var maxProfileDesc = '';
                angular.forEach(model, function (item, index) {
                        labels.push("Profile - " + (index + 1));
                        //console.log('yahoo' + points[index]);
                        if (maxscore < points[index]) {
                            maxscore = points[index];
                            maxProfileDesc = item.join();
                        }
                        dataMap.push({
                            score: points[index],
                            label: item.join()
                        })
                    })
                    //console.log('MAx Desc is :' + );
                buildService(maxProfileDesc);

                $scope.radarData.labels = labels;
                $scope.radarData.longLabels = model;
                $scope.radarData.dataMap = dataMap;
            }, function () {});

            function buildService(maxProfileDesc) {
                $http({
                    url: APIHOST + 'clients/services/' + maxProfileDesc.toUpperCase(),
                    method: 'GET'
                        //data: {"duration" : 7}
                }).then(function (response) {
                    $scope.service = response.data;
                    console.log('service data is ' + $scope.service);
                }, function (error) {
                    //alert('error');
                });
            }

        }

        $scope.listClients = function (clientType) {


            $http({
                url: APIHOST + 'clients/status=' + clientType,
                method: 'GET'
                    //data: {"duration" : 7}
            }).then(function (response) {
                $scope.data = response.data;

                angular.forEach($scope.data, function (user, key) {
                    user.score = [user.score, 100 - user.score];
                    user.scorelabel = ['Risk Measure', '']
                    user.colors = ['#EF1053', '#cccccc']

                });
                //console.log('data is : ' +  $scope.data[0].firstName);
                //$scope.tableParams = new NgTableParams({  noPager: true}, { dataset: $scope.data});

            }, function (error) {
                //alert('error');
            });
        }
        $scope.$on('$viewContentLoaded', function () {
            $timeout(function () {
                $('.moreDtls').popover({
                    html: true
                });
                //$('.moreDtls').popover('show');
            }, 500);

            $scope.listClients("NEW"); //Call Trending API on page load

        });
    });


$(document).ready(function () {

    $("body").on("click", "[id*='li-']", function () {
        var id = this.id.replace("li", "div");
        $("[id*='div-']").hide();
        console.log(id);
        $("#" + id).show();

    });

});