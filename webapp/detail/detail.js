angular.module( 'sample.detail', [
  'ui.router',
  'angular-storage',
  'angular-jwt'
])
.config(function($stateProvider) {
  $stateProvider.state('detail', {
    url: '/detail/:userId',
    controller: 'DetailCtrl',
    templateUrl: 'detail/detail.html'
  });
})
.controller( 'DetailCtrl', function LoginController( $scope, $http, store, $state, $stateParams ) {
   
  var diameter = 700,
    format = d3.format(",d"),
    color = d3.scale.category20c();
  var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);

  var svg = d3.select("#d3chart").append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");  

  var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("color", "white")
    .style("padding", "8px")
    .style("background-color", "rgba(0, 0, 0, 0.75)")
    .style("border-radius", "6px")
    .style("font", "12px sans-serif")
    .text("tooltip");

  d3.json("detail/data.json", function(error, root) {
    var node = svg.selectAll(".node")
        .data(bubble.nodes(root)
        .filter(function(d) { return !d.children; }))
      .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

    node.append("circle")
        .attr("r", function(d) { return d.r; })
        .style("fill", function(d) { return color(d.value); })
        .on("mouseover", function(d) {
                tooltip.html(d.label+"<br/>"+d.label);
                tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() {
            return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
        })
        .on("mouseout", function(){return tooltip.style("visibility", "hidden");});

    node.append("text")
        .attr("dy", ".3em")
        .style("text-anchor", "middle")
        .style("pointer-events", "none")
        .text(function(d) { return d.value; });
  });

    // Returns a flattened hierarchy containing all leaf nodes under the root.
  function classes(root) {
    var classes = [];

    function recurse(name, node) {
      if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
      else classes.push({packageName: name, className: node.name, value: node.size});
    }

    recurse(null, root);
    return {children: classes};
  }
  d3.select(self.frameElement).style("height", diameter + "px");

   var userId =  $stateParams.userId;
    $scope.radarData = {
        labels: [],
        data: [],
        options: { tooltips: {
          mode: "single",
          callbacks: {
            title: function(tooltipItem, dataObject){
              var index = tooltipItem[0].index; 
              var labels = $scope.radarData.longLabels[index];
              return labels;
            }
          }
        }}        
    }
  buildRadarChart();

  
  function buildRadarChart(){
    var url = APIHOST+"clients/metrics/"+userId;
    $http.get(url, {}).then(function(response){
        var data = response.data;
        var points = [];
        angular.forEach(data.AFFINITY, function(item){
          points.push(Math.round(item*100));
        })
        $scope.radarData.data = points;
        var model =  data.MODEL;
        var labels = []
        angular.forEach(model, function(item, index){
          labels.push("Model - "+(index+1));
        })
        $scope.radarData.labels = labels;
        $scope.radarData.longLabels = model;
      },function(){
    });
  }

    $scope.insightData = {
      labels: [],
      data: [],
      options: { 
        tooltips: {
          callbacks: {
            title: function(tooltipItem, dataObject){
              var index = tooltipItem[0].index; 
              var labels = $scope.insightData.labels[index];
              return labels;
            }
          }
        },
        scales:
        {
            xAxes: [{
                display: false
            }],
            yAxes: [{
                display: false
            }]            
        }
      }        
      
    }
  buildInsightData();  
  function buildInsightData(){
    var insightData = [];
    var url = APIHOST+"clients/model";
    $http.get(url, {}).then(function(response){
      var attributesList = response.data;
      url = APIHOST+"clients/modelDistribution"; 
      $http.get(url, {}).then(function(response){
          var attributesSizes = _.values(response.data);
          var attributeTotal = _.sum(attributesSizes);
          attributesSizes = _.map(attributesSizes, function(attributeSize){
            return Math.round((attributeSize/attributeTotal)*100);
          });
          attributesSizes = _.sortBy(attributesSizes, function(attributeSize){
            return attributeSize*-1;
          });
          var points = [];
          var point = attributesSizes[0];
          var radius = Math.round(point/2);
          var sizeFactor = 3;
          points.push({
            x: 100,
            y: 100,
            r: radius*sizeFactor
          });
          var otherPoint = attributesSizes[1];
          points.push({
            x: 100+radius+otherPoint/2,
            y: 100,
            r: (otherPoint/2)*sizeFactor
          });
          otherPoint = attributesSizes[2];
          points.push({
            x: 100,
            y: 100+radius+otherPoint/2,
            r: (otherPoint/2)*sizeFactor
          });          
          otherPoint = attributesSizes[3];
          points.push({
            x: 100-radius-otherPoint/2,
            y: 100,
            r: (otherPoint/2)*sizeFactor
          });
          otherPoint = attributesSizes[4];
          points.push({
            x: 100,
            y: 100-radius-otherPoint/2,
            r: (otherPoint/2)*sizeFactor
          });          

          $scope.insightData.data = points;
          $scope.insightData.labels = attributesList;

        })
      }, function(){
    });

  }

});
