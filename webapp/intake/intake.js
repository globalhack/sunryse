angular.module( 'sample.intake', [
  'ui.router',
  'angular-storage',
  'angular-jwt'
])
.config(function($stateProvider) {
  $stateProvider.state('intake', {
    url: '/intake/:userId',
    controller: 'IntakeCtrl',
    templateUrl: 'intake/intake.html'
  });
})
.controller( 'IntakeCtrl', function LoginController( $scope, $http, store, $state, $stateParams) {
    $scope.user = {
    };
    // housingStatus, veteran, abuse, pregnant, incomeRange, medicalProblem, troubleWithBills, familyType
    $scope.tempUser = {
    }
    var userId = $stateParams.userId;
    console.log(userId);
    var url = APIHOST+"clients/"+userId;
    $http.get(url, {}).then(function(response){
        $scope.user = response.data;
    },function(){
    });

    $scope.updateUser = function(){
        console.log($scope.tempUser);
        $scope.user.isPregnant = $scope.tempUser.pregnant;
        $scope.user.isVeteran = $scope.tempUser.veteran;
        if($scope.tempUser.medicalProblem == "physicalHealth"){
            $scope.user.isDisabled = true;
        }
        $scope.user.isAbused = $scope.tempUser.abuse;
        if($scope.tempUser.incomeRange == "none"){
            $scope.user.isEmployed = false;
        }
        if($scope.tempUser.housingStatus == "homeless"){
            $scope.user.becameHomeless = true;
        }
        console.log($scope.user);
        var url = APIHOST+"clients/"+userId;
        $http.put(url, $scope.user).then(function(response){
            console.log(response);
        }, function(error){
            console.log(error);
        })
    }

});